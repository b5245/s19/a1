let number = 2;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}`);

let address = [258, `Washington Ave NW`, `California`, `90011`];

let [streetNumber, streetName, state, postal] = address;

console.log(`I live in ${streetNumber} ${streetName} ${state} ${postal}`);

let animal = {
    name:`Lolong`,
    kind:`saltwater crocodile`,
    weight:1075,
    length:[20,3]
};

let {name, kind, weight, length} = animal;
let [feet, inch] = animal.length;

console.log(`${name} was a ${kind}. He weighed ${weight} kgs with a measurement of ${feet} ft ${inch} in.`)



let sequence = [1,2,3,4,5];
sequence.forEach((number) => console.log(number));

let reduceNumber = sequence.reduce((a,b) => a + b);
// let reduceNumber = sequence.reduce(function(a,b){
//     return a + b
//  })
console.log(reduceNumber);

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let A = new Dog(`Frankie`, 5, `Shihtzu`)

console.log(A);
